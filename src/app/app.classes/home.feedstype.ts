export interface FeedsType{
    type:string;
    sortOrder:string; 
    name:string;
    notifications:number;
}