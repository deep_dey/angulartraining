import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { NavbarComponent } from '../app/navbar/navbar.component'; 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  @ViewChild(NavbarComponent) navbar;
  selectedNav: string = ''; 

  ngAfterViewInit() { 
    setTimeout(() => {
      this.selectedNav = this.navbar.selectedNavItem;
    });
  }

  constructor() {  
  }

  recievedMessaged(event) {
    this.selectedNav = event;
  }
}

