import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; // <- for router

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { SingupComponent } from './singup/singup.component';
import { HomeComponent } from './home/home.component';
import { ForumComponent } from './forum/forum.component';
import { ChatComponent } from './chat/chat.component';
import { GalleryComponent } from './gallery/gallery.component';
import { WallComponent } from './wall/wall.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BillBoardComponent } from './bill-board/bill-board.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent, data: { title: 'Home' } },
  { path: 'forum', component: ForumComponent, data: { title: 'Forum' } },
  { path: 'forum/:id', component: ForumComponent, data: { title: 'Forum' } },
  { path: 'chat', component: ChatComponent, data: { title: 'Chat' } },
  { path: 'chat/:id', component: ChatComponent, data: { title: 'Chat' } },
  { path: 'gallery', component: GalleryComponent, data: { title: 'Gallery' } },
  { path: 'bill-board', component: BillBoardComponent, data: { title: 'Bill Board' } },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    SingupComponent,
    HomeComponent,
    ForumComponent,
    ChatComponent,
    GalleryComponent,
    WallComponent,
    PageNotFoundComponent,
    BillBoardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
