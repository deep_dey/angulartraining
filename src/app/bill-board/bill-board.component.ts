import { Component, OnInit } from '@angular/core';
import { Navs } from '../app.classes/home.nav';

@Component({
  selector: 'app-bill-board',
  templateUrl: './bill-board.component.html',
  styleUrls: ['./bill-board.component.scss']
})
export class BillBoardComponent implements OnInit {
  selectedNavItem:string;
  navItems: Array<Navs> = [
    {
      name: 'Forum',
      url: 'forum'
    },
    {
      name: 'Chats',
      url: 'chat'
    },
    {
      name: 'Gallery',
      url: 'gallery'
    } 
  ];
  
  constructor() {
    this.selectedNavItem = this.navItems[0].url;
   }

  ngOnInit() {
    
  }
  onSelect(nav) {
    this.selectedNavItem = nav; 
  }
}