import { Component, OnInit } from '@angular/core';
import { FeedsType } from '../app.classes/home.feedstype'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  selectedFeedType: string;
  FeedTypes: Array<FeedsType> = [
    {
      name: 'My Wall',
      type: 'wall',
      sortOrder: 'A',
      notifications: 10,
    },
    {
      name: 'Feeds',
      type: 'feeds',
      sortOrder: 'A',
      notifications: 12,
    }
  ];
  constructor() { 
    this.selectedFeedType = this.FeedTypes[0].type;
  }

  ngOnInit() {
  }
  onSelect(type) {
    this.selectedFeedType = type;
  }
}

