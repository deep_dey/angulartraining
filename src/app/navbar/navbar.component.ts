import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { Navs } from '../app.classes/home.nav'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Output() MessageEvent = new EventEmitter<string>();
  @Input() currentRoute: string;

  navItems: Array<Navs> = [
    {
      name: 'Home',
      url: 'home'
    },
    {
      name: 'Forum',
      url: 'forum'
    },
    {
      name: 'Chats',
      url: 'chat'
    },
    {
      name: 'Gallery',
      url: 'gallery'
    },
    {
      name: 'Bill Board',
      url: 'bill-board'
    }
  ];
  
  selectedNavItem: string = 'Home';
  onSelect(nav) {
    this.selectedNavItem = nav;
    this.MessageEvent.emit(this.selectedNavItem);
  }
  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.selectedNavItem = event.url.substring(1);
      }
      // NavigationStart
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    });
    this.MessageEvent.emit(this.selectedNavItem);
  }


  ngOnInit() {
  }


}
