import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.scss']
})
export class WallComponent implements OnInit, OnChanges {

  @Input() displayType: string;

  constructor() {
  }

  ngOnInit() { 
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.displayType.previousValue !== changes.displayType.currentValue) {
      this.displayType = changes.displayType.currentValue + ' works';
    }
  }
}
